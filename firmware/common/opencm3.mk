# opencm3 library

libopencm3.platform ?= stm32f0
libopencm3.BASEDIR ?= $(BASEDIR)../lib/opencm3/
libopencm3.CDEFS := USE_OPENCM3
libopencm3.CDIRS := $(libopencm3.BASEDIR)include
libopencm3.LDDIRS := $(libopencm3.BASEDIR)lib
libopencm3.LIB := $(libopencm3.BASEDIR)lib/libopencm3_$(libopencm3.platform).a
libopencm3.LDSCRIPTS := $(libopencm3.BASEDIR)lib/libopencm3_$(libopencm3.platform).ld

$(libopencm3.LIB) $(libopencm3.LDSCRIPTS):
	@echo TARGET libopencm3 BUILD
	$(Q)make -C $(libopencm3.BASEDIR)
