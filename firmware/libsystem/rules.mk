libsystem.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libsystem
libsystem.INHERIT := firmware libopencm3
libsystem.CDIRS := $(libsystem.BASEPATH)include
libsystem.modules ?= systick rtctick delay memtool crc
libsystem.SRCS := $(patsubst %,$(libsystem.BASEPATH)src/%.c,$(libsystem.modules))
