#ifndef DELAY_H
#define DELAY_H "delay.h"

#include <stdint.h>

typedef uint32_t delay_t;

void delay_init(void);

delay_t delay_snap(void);
void delay_wait(delay_t sc, delay_t dt);

static inline void
delay_us(delay_t us) {
  delay_t cc = delay_snap();
  delay_wait(cc, us * mcu_frequency);
}

static inline void
delay_ms(delay_t ms) {
  delay_us(ms * 1000);
}

#endif /* DELAY_H */
