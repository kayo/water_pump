typedef uint32_t rtctick_t;

#define RTCTICK_FREQUENCY 40000

void rtctick_init(void);
void rtctick_done(void);

rtctick_t rtctick_left(rtctick_t *time);

#define rtctick_to_us(time) ((time) * (1000000 / RTCTICK_FREQUENCY))
