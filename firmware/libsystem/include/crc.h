#ifndef __CRC_H__
#define __CRC_H__ "crc.h"

#include <libopencm3/stm32/crc.h>

void crc_init(void);
uint32_t crc_update(const uint8_t *ptr, uint16_t len);

#endif /* __CRC_H__ */
