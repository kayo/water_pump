#include "memtool.h"

#include <string.h>

extern char end; /* Set by linker. */
extern char _stack;

static inline void *mem_get_msp(void) {
  void *result;
  
  asm volatile ("mrs %0, msp\n\t" 
                "mov r0, %0 \n\t"
                "bx  lr     \n\t" : "=r" (result));
  
  return result;
}

void mem_prefill(void) {
  const char *msp = (const char*)mem_get_msp();
  memset(&end, MEM_PREFILL, msp - &end);
}

static const char mem_pattern[] = {
  [0 ... MEM_FREELEN-1] = MEM_PREFILL
};

void mem_measure(mem_info_t *res) {
  const char *msp = (const char*)mem_get_msp();
  const char *ptr;
  
  /* seek to beginning of pattern */
  ptr = memmem(&end, msp - &end,
               mem_pattern, sizeof(mem_pattern));
  
  res->heap_usage = ptr - &end;
  
  /* seek to end of pattern */
  ptr = memrchr(ptr, MEM_PREFILL, msp - ptr);
  
  res->stack_usage = &_stack - ptr;
}
