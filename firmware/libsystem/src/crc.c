#include "crc.h"

#ifdef STM32F1

void crc_init(void) {}

/* stm32f1 needs to reverse bits order */
static inline uint32_t rbit32(uint32_t val) {
#ifdef __thumb2__
  asm volatile("rbit %0,%0":"+r" (val):"r" (val));
#else
  asm volatile("rev %0,%0":"+r" (val):"r" (val));
  uint8_t *ptr = (uint8_t*)&val, *end = ptr + 4;
  for (; ptr < end; ptr++)
    *ptr = ((*ptr * 0x0802 & 0x22110) | (*ptr * 0x8020 & 0x88440)) * 0x10101 >> 16;
#endif
  return val;
}

uint32_t crc_update(const uint8_t *ptr, uint16_t len) {
  //  uint32_t l = len / 4, e = len & 3;
  const uint32_t *buf = (const uint32_t*)ptr;
  uint32_t crc = 0, crc_ = CRC_DR;
  
  for (; len >= 4; len -= 4)
    crc_ = crc_calculate(rbit32(*buf++));
  
  crc = rbit32(crc_);
  
  if (len > 0) {
    uint8_t len8 = len << 3;
    
    /* reset crc data register (=0) */
    crc_calculate(crc_);
    
    crc = (crc >> len8) ^ rbit32( crc_calculate( rbit32( (*buf & ((1 << len8) - 1)) ^ crc ) >> (32 - len8) ) );
  }
  
  return ~crc;
}

#else

void crc_init(void) {
  CRC_CR = CRC_CR_REV_IN_WORD;
}

uint32_t crc_update(const uint8_t *ptr, uint16_t len) {
  //  uint32_t l = len / 4, e = len & 3;
  const uint32_t *buf = (const uint32_t*)ptr;
  uint32_t crc = 0, crc_ = CRC_DR;
  
  for (; len >= 4; len -= 4)
    crc_ = crc_calculate(*buf++);
  
  crc = crc_;
  
  if (len > 0) {
    uint8_t len8 = len << 3;
    
    /* reset crc data register (=0) */
    crc_calculate(crc_);
    
    crc = (crc >> len8) ^ crc_calculate( (*buf & ((1 << len8) - 1)) ^ crc ) >> (32 - len8);
  }
  
  return ~crc;
}

#endif
