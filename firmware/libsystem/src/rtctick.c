#include <libopencm3/stm32/rtc.h>

#include "macro.h"
#include "config.h"
#include "rtctick.h"

void rtctick_init(void) {
  /* 40kHz */
  rtc_auto_awake(RCC_LSI, 0);
}

void rtctick_done(void) {
  
}

rtctick_t rtctick_left(rtctick_t *time) {
  rtctick_t _time = *time;
  
  *time = rtc_get_counter_val();
  
  return *time >= _time ? *time - _time : UINT32_MAX - _time + *time;
}
