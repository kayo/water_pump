#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>

#include "systick.h"
#include "macro.h"
#include "config.h"

#ifndef systick_config
#error "Please, add `#define systick_config <divider>,<period>[,<ext_freq>]`. Where `divider` is 1 or 8 (or 0 if external clock source is used), `period` is number of milliseconds, `ext_freq` is external source frequency in MHz."
#endif

#if _NTH0(systick_config) == 0
#define SYSTICK_DIVIDER STK_CSR_CLKSOURCE_EXT
#define SYSTICK_PERIOD (_NTH2(systick_config)*_NTH1(systick_config)*1000)
#else
#if _NTH0(systick_config) == 1
#define SYSTICK_DIVIDER STK_CSR_CLKSOURCE_AHB
#else
#define SYSTICK_DIVIDER _CAT2(STK_CSR_CLKSOURCE_AHB_DIV, _NTH0(systick_config))
#endif
#define SYSTICK_PERIOD ((mcu_frequency)/_NTH0(systick_config)*_NTH1(systick_config)*1000)
#endif

#if SYSTICK_PERIOD > (1<<24)
#error "systick period too big"
#endif

void systick_init(void) {
  /* Enable systick interrupt */
	nvic_enable_irq(NVIC_SYSTICK_IRQ);
  
  /* 72MHz / 8 = 9MHz counts per second */
	systick_set_clocksource(SYSTICK_DIVIDER);
  
  /* SYSTICK_PERIOD = systick_interval * 9MHz */
	systick_set_reload(SYSTICK_PERIOD-1);
  
  /* Enable systick interrupt */
	systick_interrupt_enable();
  
  /* Enable systick counter for monitor step function */
	systick_counter_enable();
}

void systick_done(void) {
  /* Disable sys tick counter for monitor step function */
  systick_counter_disable();
  systick_interrupt_disable();
  
  /* Disable systick interrupt */
	nvic_disable_irq(NVIC_SYSTICK_IRQ);
}
