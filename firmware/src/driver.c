#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>

#include <math.h>

#include "sensor.h"
#include "driver.h"
#include "macro.h"
#include "config.h"

#define ZERO_DET_PORT _CAT2(GPIO, _NTH0(zero_det_input))
#define ZERO_DET_PAD _CAT2(GPIO, _NTH1(zero_det_input))
#define ZERO_DET_EXTI _CAT2(EXTI, _NTH1(zero_det_input))
#define ZERO_DET_IRQ _CAT3(NVIC_EXTI, _NTH1(zero_det_input), _IRQ)
#define zero_det_isr _CAT3(exti, _NTH1(zero_det_input), _isr)

#define PUMP_CTRL_PORT _CAT2(GPIO, _NTH0(pump_ctrl_output))
#define PUMP_CTRL_PAD _CAT2(GPIO, _NTH1(pump_ctrl_output))
#define PUMP_CTRL_TIMER_RCC _CAT2(RCC_TIM, _NTH0(pump_ctrl_timer))
#define PUMP_CTRL_TIMER _CAT2(TIM, _NTH0(pump_ctrl_timer))
#define PUMP_CTRL_TIMER_OC _CAT2(TIM_OC, _NTH1(pump_ctrl_timer))
#define PUMP_CTRL_TIMER_IRQ _CAT3(NVIC_TIM, _NTH0(pump_ctrl_timer), _IRQ)
#define pump_ctrl_timer_isr _CAT3(tim, _NTH0(pump_ctrl_timer), _isr)

#define PUMP_CTRL_BITS 12
#define PUMP_CTRL_WIDTH ((1<<(PUMP_CTRL_BITS))-1)
#define PUMP_CTRL_FREQ 100
#define PUMP_CTRL_DIV ((uint16_t)((1e6 * (mcu_frequency) / PUMP_CTRL_FREQ / (1<<(PUMP_CTRL_BITS)) * 2 + 1) / 2))
#define PUMP_CTRL_PERIOD ((uint16_t)(1e6 * (mcu_frequency) / PUMP_CTRL_FREQ / PUMP_CTRL_DIV))

void driver_init(void) {
  /* Enable EXTI0 interrupt */
  nvic_enable_irq(ZERO_DET_IRQ);

  /* Setup zero detector input */
  gpio_set_mode(ZERO_DET_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                ZERO_DET_PAD);
  /* pull-up */
  gpio_set(ZERO_DET_PORT, ZERO_DET_PAD);

  /* Configure the EXTI subsystem */
  exti_select_source(ZERO_DET_EXTI, ZERO_DET_PORT);
  exti_set_trigger(ZERO_DET_EXTI, EXTI_TRIGGER_FALLING);
  exti_enable_request(ZERO_DET_EXTI);

  /* Enable timer clock */
  rcc_periph_clock_enable(PUMP_CTRL_TIMER_RCC);

  /* Enable timer interrupt */
  nvic_enable_irq(PUMP_CTRL_TIMER_IRQ);

  /* Configure pump control timer */
  timer_reset(PUMP_CTRL_TIMER);

  /* Timer global mode */
  timer_set_mode(PUMP_CTRL_TIMER,
                 TIM_CR1_CKD_CK_INT, /* no divider */
                 TIM_CR1_CMS_EDGE, /* alignment edge */
                 TIM_CR1_DIR_DOWN); /* direction up */
  
  /* Set prescaler value */
  timer_set_prescaler(PUMP_CTRL_TIMER, PUMP_CTRL_DIV - 1);

  /* Enable preload */
  timer_enable_preload(PUMP_CTRL_TIMER);

  /* Continous mode */
  timer_continuous_mode(PUMP_CTRL_TIMER);

  /* Set period */
  timer_set_period(PUMP_CTRL_TIMER, PUMP_CTRL_PERIOD);

  /* We need validation pump control synchronization with HV wave */
  /* Enable update interrupt */
  timer_enable_irq(PUMP_CTRL_TIMER, TIM_DIER_UIE);

  /* Disable outputs */
  timer_disable_oc_output(PUMP_CTRL_TIMER, TIM_OC1);
  timer_disable_oc_output(PUMP_CTRL_TIMER, TIM_OC2);
  timer_disable_oc_output(PUMP_CTRL_TIMER, TIM_OC3);
  timer_disable_oc_output(PUMP_CTRL_TIMER, TIM_OC4);

  /* Configure global mode of line */
  //timer_enable_oc_clear(PUMP_CTRL_TIMER, PUMP_CTRL_TIMER_OC);
  timer_enable_oc_preload(PUMP_CTRL_TIMER, PUMP_CTRL_TIMER_OC);
  //timer_disable_oc_preload(PUMP_CTRL_TIMER, PUMP_CTRL_TIMER_OC);
  timer_set_oc_fast_mode(PUMP_CTRL_TIMER, PUMP_CTRL_TIMER_OC);
  timer_set_oc_mode(PUMP_CTRL_TIMER, PUMP_CTRL_TIMER_OC, TIM_OCM_PWM1);
  timer_set_oc_polarity_low(PUMP_CTRL_TIMER, PUMP_CTRL_TIMER_OC);
  timer_enable_oc_output(PUMP_CTRL_TIMER, PUMP_CTRL_TIMER_OC);

  /* Turn off pump */
  driver_pump_power(0);

  /* To avoid low level pulse on pump control output we need:
	 1. Enable timer counter
	 2. Configure altfn output
	 3. Disable timer counter
  */
  timer_enable_counter(PUMP_CTRL_TIMER);
#if 1
  gpio_set_mode(PUMP_CTRL_PORT,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN,
                PUMP_CTRL_PAD);
#endif
  timer_disable_counter(PUMP_CTRL_TIMER);
}

void driver_done(void) {
  exti_disable_request(ZERO_DET_EXTI);

  /* Disable timer update interrupt */
  timer_disable_irq(PUMP_CTRL_TIMER, TIM_DIER_UIE);

  /* Disable timer interrupt */
  nvic_disable_irq(PUMP_CTRL_TIMER_IRQ);

  /* Disable EXTI0 interrupt */
  nvic_disable_irq(ZERO_DET_IRQ);
}

static volatile uint8_t hv_periods = 0;

void zero_det_isr(void) {
  exti_reset_request(ZERO_DET_EXTI);

  hv_periods = 0;
  timer_set_counter(PUMP_CTRL_TIMER, 0);
  timer_enable_counter(PUMP_CTRL_TIMER);
  
  /* Trigger sensor */
  sensor_resume();
}

void pump_ctrl_timer_isr(void) {
  timer_clear_flag(PUMP_CTRL_TIMER, TIM_SR_UIF);
  
  if (++hv_periods == 3) {
	timer_disable_counter(PUMP_CTRL_TIMER);
  }
}

void driver_pump_power(float val) {
  /* linear power control */
  /*
	Q[U]: sin(%pi * t);
	Q[P]: (1 - cos(%pi * t)) / 2;
	wxplot2d([Q[U], Q[P]], [t, 0, 1]);
	Q[C]: rhs(solve(x = Q[P], t)[1]);
	wxplot2d([Q[C]], [x, 0, 1]);
  */
  /*
	EQ[p]: p=sin(%pi*t)^2;
	wxplot2d([rhs(EQ[p])], [t, 0, 1]);
	EQ[t]: solve(EQ[p], t)[2];
	wxplot2d([2*rhs(EQ[t])], [p, 0, 1]);
   */
  val = 1.0 - acos(2 * val - 1) / M_PI;
  
  uint16_t cmp = val * PUMP_CTRL_PERIOD;
  
  if (cmp >= PUMP_CTRL_PERIOD) {
    cmp = PUMP_CTRL_PERIOD - 1;
  }
  
  /* Set the capture compare value for OC */
  timer_set_oc_value(PUMP_CTRL_TIMER, PUMP_CTRL_TIMER_OC, cmp);
}
