#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/sync.h>

#include <stdlib.h>
#include <math.h>

#include "sensor.h"
#include "macro.h"
#include "config.h"

/* hardware defines */
#define P_WATER_PORT _CAT2(GPIO, _NTH0(water_press_input))
#define P_WATER_PAD _CAT2(GPIO, _NTH1(water_press_input))
#define P_WATER_ADC_CH _CAT2(ADC_CHANNEL, _NTH1(water_press_adc))

#define T_WATER_PORT _CAT2(GPIO, _NTH0(water_therm_input))
#define T_WATER_PAD _CAT2(GPIO, _NTH1(water_therm_input))
#define T_WATER_ADC_CH _CAT2(ADC_CHANNEL, _NTH1(water_therm_adc))

#define T_BOX_PORT _CAT2(GPIO, _NTH0(box_therm_input))
#define T_BOX_PAD _CAT2(GPIO, _NTH1(box_therm_input))
#define T_BOX_ADC_CH _CAT2(ADC_CHANNEL, _NTH1(box_therm_adc))

/* ADC filtering and conversion */

#ifndef USE_AVERAGE_FILTER
#define USE_AVERAGE_FILTER 1
#endif

#ifndef USE_MEDIAN_FILTER
#define USE_MEDIAN_FILTER 1
#endif

#ifndef ADC_SELECT
#define ADC_SELECT 8
#endif

#ifndef ADC_SAMPLES
#define ADC_SAMPLES 24
#endif

#ifndef ADC_BITWIDTH
#define ADC_BITWIDTH 12
#endif

#ifndef ADC_REFERENCE
#define ADC_REFERENCE 1.2
#endif

#ifndef ADC_VOLTAGE
#define ADC_VOLTAGE 3.3
#endif

#define ADC_MAXVALUE ((adc_value_t)((1 << ADC_BITWIDTH) - 1))

#define ADC_REFVALUE ((adc_value_t)(ADC_REFERENCE * ADC_MAXVALUE / 3.3))

typedef uint16_t adc_value_t;

typedef struct {
  adc_value_t Tmcu;
  adc_value_t Vref;
  adc_value_t Pwater;
  adc_value_t Twater;
  adc_value_t Tbox;
} adc_values_t;

static adc_values_t adc_buffer[ADC_SAMPLES];

#define ADC_CHANNELS (sizeof(adc_values_t)/sizeof(adc_value_t))

static adc_value_t adc_value_sample(const adc_value_t *field) {
#if USE_MEDIAN_FILTER
  adc_value_t values[ADC_SAMPLES], *value = values, *end = values + ADC_SAMPLES;
  
  for(; value < end; ){
    *value++ = *field;
    field += ADC_CHANNELS;
  }

  int adc_comp(const void *a, const void *b) {
    return *(const adc_value_t*)a < *(const adc_value_t*)b ? -1 : *(const adc_value_t*)a > *(const adc_value_t*)b ? 1 : 0;
  }
  
  qsort(values, ADC_SAMPLES, sizeof(adc_value_t), adc_comp);
  
#if USE_AVERAGE_FILTER
  uint32_t accum = 0;
  
  value = values + (ADC_SAMPLES-ADC_SELECT)/2;
  end = value + ADC_SELECT;
  
  for(; value < end; ){
    accum += *value++;
  }
  
  return accum / ADC_SELECT;
#else
  return values[ADC_SAMPLES / 2 + 1];
#endif

#else
  uint64_t accum = 0;
  int i;
  
  for(i = 0; i < ADC_SAMPLES; i++){
    accum += *field;
    field += ADC_CHANNELS;
  }

  return accum / ADC_SAMPLES;
#endif
}

static mutex_t adc_mutex;
static adc_values_t adc_values;

static void adc_convert(void) {
  mutex_lock(&adc_mutex);
  adc_values.Tmcu = adc_value_sample(&adc_buffer->Tmcu);
  adc_values.Vref = adc_value_sample(&adc_buffer->Vref);
  adc_values.Pwater = adc_value_sample(&adc_buffer->Pwater);
  adc_values.Twater = adc_value_sample(&adc_buffer->Twater);
  adc_values.Tbox = adc_value_sample(&adc_buffer->Tbox);
  mutex_unlock(&adc_mutex);
}

static adc_value_t adc_value(const adc_value_t *field) {
  mutex_lock(&adc_mutex);
  adc_value_t value = *((const adc_value_t*)&adc_values + (field - (const adc_value_t*)&adc_buffer));
  mutex_unlock(&adc_mutex);
  return value;
}

static float adc_fract(const adc_value_t *field) {
  return (float) adc_value(field) / ADC_MAXVALUE;
}

float sensor_Vref(void) {
  return adc_fract(&adc_buffer->Vref) * ADC_VOLTAGE;
}

/* (v / ADC_MAXVALUE - a / ADC_VOLTAGE) / ((b - a) / ADC_VOLTAGE) */
/* (v - a * ADC_MAXVALUE / ADC_VOLTAGE) / ((b - a) * ADC_MAXVALUE / ADC_VOLTAGE) */
#define adc_convert_range(v, a, b, l, h) ((((float)(v) - ((float)(a) * ADC_MAXVALUE / ADC_VOLTAGE)) * (((float)(h) - (float)(l)) / (((float)(b) - (float)(a)) * ADC_MAXVALUE / ADC_VOLTAGE))) + (float)(l))

float sensor_Pwater(void) {
  return adc_convert_range(adc_value(&adc_buffer->Pwater),
                           _NTH0(_UNWR(P_WATER_RANGE)),
                           _NTH1(_UNWR(P_WATER_RANGE)),
                           _NTH2(_UNWR(P_WATER_RANGE)),
                           _NTH3(_UNWR(P_WATER_RANGE)));
}

#ifndef VSENSE_25
#define VSENSE_25 1.41
#endif

#ifndef AVG_SLOPE
#define AVG_SLOPE 4.3
#endif

float sensor_Tmcu(void) {
  return (VSENSE_25 - adc_fract(&adc_buffer->Tmcu)) / AVG_SLOPE + 25.0;
}

typedef struct {
  double a;
  double b;
  double c;
} sh_t;

static double shR2K(const sh_t *p, double r){
  double l = logf(r);
  return 1.0 / (p->a + p->b * l + p->c * pow(l, 3));
}

typedef struct {
  double beta;
  double r0;
  double t0;
} beta_t;

static double betaR2K(const beta_t *p, double r){
  return 1.0 / (1.0 / p->t0 + 1.0 / p->beta * logf(r / p->r0));
}

static inline float K2C(float K){
  return K - 273.15;
}

/**
 * @brief Convert ADC value to resistance.
 *
 *        Analog VCC
 *
 *            ^
 *            |
 *           .-.
 *           | |
 *        R1 | |
 *           | |
 *           '-'
 *            |
 *        .---o---.
 *        |       |
 *       .-.     .-.
 *       | |     | |
 *    R2 | |  Rt | |
 *       | |     | |
 *       '-'     '-'
 *        |       |
 *        '---o---'
 *            |
 *           _|_
 *
 *        Analog GND
 *
 * If r2 doesn't used, r2 must be set to 0
 */

#ifndef TSENSE_R1
#define TSENSE_R1 4700
#endif

#ifndef TSENSE_R2
#define TSENSE_R2 0
#endif

static float A2R(uint32_t r1, uint32_t r2, float a){
  return r2 > 0 ? a * r1 * r2 / ((1 - a) * r2 - a * r1) : a * r1 / (1 - a);
}

/* 100K NTC */
static const sh_t shp = {
  0.0013081992342927878,
  0.00005479611105636211,
  9.415113523239857e-7,
};

static float Tsens_100K(const adc_value_t *field){
  return K2C(shR2K(&shp, A2R(TSENSE_R1, TSENSE_R2, adc_fract(field))));
}

/* 10K NTC */
static const beta_t betap = {
  3450,
  10e3,
  24 + 273.15,
};

static float Tsens_10K(const adc_value_t *field){
  return K2C(betaR2K(&betap, A2R(TSENSE_R1, TSENSE_R2, adc_fract(field))));
}

float sensor_Twater(void){
  return Tsens_100K(&adc_buffer->Twater);
}

float sensor_Tbox(void){
  return Tsens_10K(&adc_buffer->Tbox);
}

void sensor_resume(void) {
  adc_start_conversion_direct(ADC1);
}

void sensor_suspend(void) {
  adc_power_off(ADC1);
  adc_power_on(ADC1);
}

void sensor_init(void) {
  /* Enable ADC1 clock */
  //rcc_periph_clock_enable(RCC_ADC1);
  
  /* Enable DMA1 channel1 interrupt */
  nvic_enable_irq(NVIC_DMA1_CHANNEL1_IRQ);

  /* Configure sensor GPIO */
	gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                P_WATER_PAD
                | T_WATER_PAD
                | T_BOX_PAD
                );

  /* Configure ADC subsystem */
  adc_power_off(ADC1);
  
  /* ADC frequency (ADCCLK) is 24MHz/2 = 12MHz and period is 1/12 uS */
  
  /* ADC frequency (ADCCLK) is 24MHz/4 = 6MHz and period is 1/6 uS */
  /*rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV4);*/
  
  /* ADC frequency (ADCCLK) is 24MHz/8 = 3MHz and period is 1/3 uS */
  /*rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV8);*/
  
  /*adc_enable_external_trigger_regular(ADC1, ADC_CR2_EXTSEL_SWSTART);*/
  adc_set_continuous_conversion_mode(ADC1);
  adc_set_right_aligned(ADC1);
  
  /* ADC conversion time for Tmcu (Tconv) is 239.5 + 12.5 = 252 cycles */
  /* 252 cycles / 12 MHz = 21 uS > 17.1 uS is ok */
  adc_set_sample_time(ADC1, ADC_CHANNEL_TEMP, ADC_SMPR_SMP_239DOT5CYC); /*Tmcu*/
  
  /* ADC conversion time for Tmcu (Tconv) is 28.5 + 12.5 = 41 cycles */
  /* 41 cycles / 12 MHz = 3.4 uS */
  adc_set_sample_time(ADC1, ADC_CHANNEL_VREF, ADC_SMPR_SMP_28DOT5CYC); /*Vref*/

  /* ADC conversion time for Tmcu (Tconv) is 71.5 + 12.5 = 84 cycles */
  /* 84 cycles / 12 MHz = 7 uS */
  adc_set_sample_time(ADC1, P_WATER_ADC_CH, ADC_SMPR_SMP_71DOT5CYC);
  
  /* ADC conversion time for Tmcu (Tconv) is 55.5 + 12.5 = 68 cycles */
  /* 68 cycles / 12 MHz = 5.7 uS */
  adc_set_sample_time(ADC1, T_WATER_ADC_CH, ADC_SMPR_SMP_55DOT5CYC);
  adc_set_sample_time(ADC1, T_BOX_ADC_CH, ADC_SMPR_SMP_55DOT5CYC);
  
  adc_enable_temperature_sensor(ADC1);
  adc_enable_scan_mode(ADC1);
  {
    uint8_t channel[] = { ADC_CHANNEL_TEMP,
                          ADC_CHANNEL_VREF,
                          P_WATER_ADC_CH,
                          T_WATER_ADC_CH,
                          T_BOX_ADC_CH };
    adc_set_regular_sequence(ADC1, sizeof(channel)/sizeof(channel[0]), channel);
  }

  adc_power_on(ADC1);

  { int i;
    for (i = 0; i < 800000; i++)    /* Wait a bit. */
      __asm__("nop");
  }

  adc_reset_calibration(ADC1);
  adc_calibration(ADC1);
  
  adc_enable_dma(ADC1);
  
  dma_channel_reset(DMA1, DMA_CHANNEL1);
  dma_set_priority(DMA1, DMA_CHANNEL1, DMA_CCR_PL_VERY_HIGH);
  dma_set_peripheral_size(DMA1, DMA_CHANNEL1, DMA_CCR_PSIZE_16BIT);
  dma_set_memory_size(DMA1, DMA_CHANNEL1, DMA_CCR_MSIZE_16BIT);
  dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL1);
  dma_enable_circular_mode(DMA1, DMA_CHANNEL1);
  dma_set_read_from_peripheral(DMA1, DMA_CHANNEL1);
  dma_set_peripheral_address(DMA1, DMA_CHANNEL1, (uint32_t)&ADC_DR(ADC1));
  dma_set_memory_address(DMA1, DMA_CHANNEL1, (uint32_t)&adc_buffer);
  dma_set_number_of_data(DMA1, DMA_CHANNEL1, sizeof(adc_buffer)/sizeof(adc_value_t));
  
  /* Enable transfer complete interrupt */
  dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL1);

  /* Start DMA transfer */
  dma_enable_channel(DMA1, DMA_CHANNEL1);
}

void sensor_done(void) {
  /* Disable the ADC1 */
  adc_power_off(ADC1);

  /*
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                P_WATER_PAD
                | T_WATER_PAD
                | T_BOX_PAD
                );
  */
  
  /* Disable the DMA1 channel1 */
  dma_disable_transfer_complete_interrupt(DMA1, DMA_CHANNEL1);
  dma_disable_channel(DMA1, DMA_CHANNEL1);

  /* Disable DMA1 channel1 interrupt */
  nvic_disable_irq(NVIC_DMA1_CHANNEL1_IRQ);
  
  /* Disable ADC1 clock */
  //rcc_periph_clock_disable(RCC_ADC1);
}

/* Initial adc conversion complete */
void dma1_channel1_isr(void) {
  dma_clear_interrupt_flags(DMA1, DMA_CHANNEL1, DMA_GIF | DMA_TCIF);

  sensor_suspend();
  adc_convert();
}
