#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/crc.h>
#include <libopencm3/cm3/nvic.h>

#include <stdlib.h>

#include "macro.h"
#include "config.h"

#define UART_NUM _NTH0(console_uart)
#define UART_RE _CAT3(uart, UART_NUM, _remap)

#define UART_DEV _CAT2(USART, UART_NUM)
#define UART_RCC _CAT2(RCC_USART, UART_NUM)

#define BAUDRATE _NTH1(console_uart)
#define DATABITS _NTH2(console_uart)
#define STOPBITS _CAT2(USART_STOPBITS_, _NTH3(console_uart))

#define TX_PORT _CAT4(GPIO_BANK_USART, UART_NUM, UART_RE, _TX)
#define TX_PAD _CAT4(GPIO_USART, UART_NUM, UART_RE, _TX)
#define RX_PORT _CAT4(GPIO_BANK_USART, UART_NUM, UART_RE, _RX)
#define RX_PAD _CAT4(GPIO_USART, UART_NUM, UART_RE, _RX)

#define DMA_NUM _NTH0(console_dma)

#define DMA_DEV _CAT2(DMA, DMA_NUM)
#define DMA_TX_CH _CAT2(DMA_CHANNEL, _NTH1(console_dma))
#define DMA_RX_CH _CAT2(DMA_CHANNEL, _NTH2(console_dma))

#define TX_IRQ _CAT5(NVIC_DMA, DMA_NUM, _CHANNEL, _NTH1(console_dma), _IRQ)
#define tx_isr _CAT5(dma, DMA_NUM, _channel, _NTH1(console_dma), _isr)

#include "param.h"
#include "console.h"
#include "param-iface.h"
#include "param-json.h"

#ifndef CONSOLE_TX_SIZE
#define CONSOLE_TX_SIZE 1024
#endif

#ifndef CONSOLE_RX_SIZE
#define CONSOLE_RX_SIZE 1024
#endif

#ifndef CONSOLE_JSON
#define CONSOLE_JSON 1
#endif

static union {
  uint8_t rx[CONSOLE_RX_SIZE];
  uint8_t tx[CONSOLE_TX_SIZE];
} io;

static void tx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_TX_CH);
}

static void tx_enable(uint16_t size) {
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_TX_CH, size);
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_TX_CH);
}

static void rx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_RX_CH);
}

static void rx_enable(void) {
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_RX_CH, sizeof(io.rx));
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_RX_CH);
}

void console_init(void) {
  /* Enable USART clock */
  rcc_periph_clock_enable(UART_RCC);

#if !CONSOLE_JSON
  /* Enable CRC unit clock */
  rcc_periph_clock_enable(RCC_CRC);
#endif

  /* Enable DMA_DEV TX channel interrupt */
  nvic_enable_irq(TX_IRQ);

  /* Setup GPIO pin TX for transmit */
	gpio_set_mode(TX_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, TX_PAD);
  
  /* Setup GPIO pin RX for receive */
	gpio_set_mode(RX_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, RX_PAD);
  
  /* Setup USART parameters */
	usart_set_baudrate(UART_DEV, BAUDRATE);
	usart_set_databits(UART_DEV, DATABITS);
	usart_set_stopbits(UART_DEV, STOPBITS);
	usart_set_parity(UART_DEV, USART_PARITY_NONE);
	usart_set_flow_control(UART_DEV, USART_FLOWCONTROL_NONE);
  usart_set_mode(UART_DEV, USART_MODE_TX_RX);

  /* Setup USART DMA transfer */
  
  /* TX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_TX_CH);
  dma_set_priority(DMA_DEV, DMA_TX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_TX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_TX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_TX_CH);
  dma_set_read_from_memory(DMA_DEV, DMA_TX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_TX_CH, (uint32_t)&USART_DR(UART_DEV));
  dma_set_memory_address(DMA_DEV, DMA_TX_CH, (uint32_t)io.tx);
  
  /* RX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_RX_CH);
  dma_set_priority(DMA_DEV, DMA_RX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_RX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_RX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_RX_CH);
  dma_enable_circular_mode(DMA_DEV, DMA_RX_CH);
  dma_set_read_from_peripheral(DMA_DEV, DMA_RX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_RX_CH, (uint32_t)&USART_DR(UART_DEV));
  dma_set_memory_address(DMA_DEV, DMA_RX_CH, (uint32_t)io.rx);
  
  /* Enable TX transfer complete interrupt */
  dma_enable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);

  /* Enable TX transfer error interrupt */
  /*dma_enable_transfer_error_interrupt(DMA_DEV, DMA_TX_CH);*/
  
  usart_enable_tx_dma(UART_DEV);
  usart_enable_rx_dma(UART_DEV);
  
	/* Enable the USART */
	usart_enable(UART_DEV);
  
  rx_enable();
}

void console_done(void) {
  /* Disable the USART */
	usart_disable(UART_DEV);
  
  usart_disable_tx_dma(UART_DEV);
  usart_disable_rx_dma(UART_DEV);
  
  /* Disable the DMA_DEV channel4 and channel5 */
  dma_disable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);
  /*dma_disable_transfer_error_interrupt(DMA_DEV, DMA_TX_CH);*/
  dma_disable_channel(DMA_DEV, DMA_TX_CH);
  dma_disable_channel(DMA_DEV, DMA_RX_CH);
  
  /* Disable DMA_DEV channel4 interrupt */
  nvic_disable_irq(TX_IRQ);

#if !CONSOLE_JSON
  /* Disable CRC unit clock */
  rcc_periph_clock_disable(RCC_CRC);
#endif

  /* Disable USART clock */
  rcc_periph_clock_disable(UART_RCC);
}

void tx_isr(void) {
  /*bool error = dma_get_interrupt_flag(DMA_DEV, DMA_TX_CH, DMA_TEIF);*/
  dma_clear_interrupt_flags(DMA_DEV, DMA_TX_CH, DMA_GIF | DMA_TEIF | DMA_TCIF);
  tx_disable();
  rx_enable();
}

static inline uint32_t dma_is_active(uint32_t dma, uint8_t channel) {
  return DMA_CCR(dma, channel) & DMA_CCR_EN;
}

static inline uint32_t dma_count_data(uint32_t dma, uint8_t channel) {
  return DMA_CNDTR(dma, channel);
}

#if !CONSOLE_JSON
static inline uint32_t rbit32(uint32_t val) {
  asm volatile("rbit %0,%0":"+r" (val):"r" (val));
  return val;
}

static uint32_t crc_update(const uint8_t *ptr, uint16_t len) {
  //  uint32_t l = len / 4, e = len & 3;
  const uint32_t *buf = (const uint32_t*)ptr;
  uint32_t crc = 0, crc_ = CRC_DR;
  
  for (; len >= 4; len -= 4)
    crc_ = crc_calculate(rbit32(*buf++));
  
  crc = rbit32(crc_);
  
  if (len > 0) {
    uint8_t len8 = len << 3;
    
    /* reset crc data register (=0) */
    crc_calculate(crc_);
    
    crc = (crc >> len8) ^ rbit32( crc_calculate( rbit32( (*buf & ((1 << len8) - 1)) ^ crc ) >> (32 - len8) ) );
  }
  
  return ~crc;
}
#endif

void console_step(const param_coll_t *params) {
  if (!dma_is_active(DMA_DEV, DMA_RX_CH) || !usart_get_flag(UART_DEV, USART_SR_IDLE)) return;
  
  if (usart_get_flag(UART_DEV, USART_SR_NE | USART_SR_FE)) {
    rx_disable();
    rx_enable();
    return;
  }
  
  param_size_t size = sizeof(io.rx) - dma_count_data(DMA_DEV, DMA_RX_CH);
  
#if CONSOLE_JSON
  /* json text interface */
  if (size > 0) {
    int res = param_json_handle(params, (const char*)io.rx, (const char*)io.rx + size, (char*)io.tx, (char*)io.tx + sizeof(io.tx));
    
    if (res > 0) {
      rx_disable();
      tx_enable(res);
    }
  }
  
#else
  /* binary interface */
  const param_size_t *ilen = (const param_size_t *)io.rx;
  
  if (size < sizeof(*ilen)) return;

  const param_cell_t *iptr = io.rx + sizeof(*ilen);
  const uint32_t *icrc = (const uint32_t*)iptr + *ilen;
  
  param_size_t *olen = (param_size_t *)io.tx;
  param_cell_t *optr = io.tx + sizeof(*olen);

  *olen = sizeof(io.tx);
  
  if (*ilen < sizeof(io.tx) - sizeof(*ilen) - sizeof(*icrc)) {
    if (size < sizeof(*ilen) + *ilen + sizeof(*icrc)) return;
    
    crc_reset();
    
    if (crc_update(iptr, *ilen) == *icrc) {
      param_iface_handle(params, iptr, *ilen, optr, olen);
    } else {
      *olen = 0;
    }
  } else {
    *olen = 0;
  }
  
  uint32_t *ocrc = (uint32_t*)optr + *olen;
  
  crc_reset();
  *ocrc = crc_update(optr, *olen);
  
  rx_disable();
  tx_enable(sizeof(*olen) + *olen + sizeof(*ocrc));
#endif
}
