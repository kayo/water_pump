#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>

#include "flowout.h"
#include "macro.h"
#include "config.h"

#define TIMER_DEV _CAT2(TIM, _NTH0(water_flow_timer))
#define TIMER_RCC _CAT2(RCC_TIM, _NTH0(water_flow_timer))

static inline void timer_external_clock_enable(uint32_t timer_peripheral) {
  TIM_SMCR(timer_peripheral) |= TIM_SMCR_ECE;
}

static inline void timer_external_clock_disable(uint32_t timer_peripheral) {
  TIM_SMCR(timer_peripheral) &= ~TIM_SMCR_ECE;
}

void flowout_init(void) {
	rcc_periph_clock_enable(TIMER_RCC);

  timer_reset(TIMER_DEV);

  /* Timer global mode:
	 * - No divider
	 * - Alignment edge
	 * - Direction up
	 */
	timer_set_mode(TIMER_DEV, TIM_CR1_CKD_CK_INT,
                 TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

  timer_set_prescaler(TIMER_DEV, 0);
	timer_disable_preload(TIMER_DEV);
	timer_one_shot_mode(TIMER_DEV);
	timer_set_period(TIMER_DEV, 65535);

  timer_external_clock_enable(TIMER_DEV);
  timer_slave_set_prescaler(TIMER_DEV, TIM_IC_PSC_OFF);
  timer_slave_set_filter(TIMER_DEV, TIM_IC_OFF);
  timer_slave_set_polarity(TIMER_DEV, TIM_ET_RISING);
  
	timer_enable_counter(TIMER_DEV);
}

void flowout_done(void) {
  timer_disable_counter(TIMER_DEV);
  
  rcc_periph_clock_disable(TIMER_RCC);
}

/*
 * 10 liters/min ~ 1/6 liter/sec ~ 82 Hz ~ 12.195 mS
 * 1 liter/min ~ 8.2 Hz ~ 121.951 mS
 */

//#define FLOW_UNIT ((float)(1.0 / (6.0 * 82.0)))
#define FLOW_UNIT ((float)(1.0 / (6.0 * 53.3)))

float flowout_stat(void) {
  uint16_t count = timer_get_counter(TIMER_DEV);
  
  timer_set_counter(TIMER_DEV, 0);

  return FLOW_UNIT * (float)count;
}
