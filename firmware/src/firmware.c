#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>

#include <stddef.h>
#include <stdbool.h>

#include "macro.h"
#include "config.h"
#include "delay.h"
#include "systick.h"
#include "monitor.h"
#include "sensor.h"
#include "driver.h"
#include "flowout.h"
#include "params.h"
#include "console.h"
#include "pid_def.h"
#include "kalman.h"
#include "filter.h"
#include "ewma.h"

#ifndef FAST_INIT
#define FAST_INIT 0
#endif

#ifndef HAS_CONSOLE
#define HAS_CONSOLE 1
#endif

static void init(void) {
  /* Setup MCU clock */
  rcc_clock_setup_in_hsi_out_24mhz();

#if FAST_INIT
  rcc_peripheral_enable_clock(&RCC_AHBENR,
                              RCC_AHBENR_DMA1EN);
  rcc_peripheral_enable_clock(&RCC_APB2ENR,
                              RCC_APB2ENR_IOPAEN
                              | RCC_APB2ENR_IOPBEN
                              | RCC_APB2ENR_AFIOEN
                              | RCC_APB2ENR_ADC1EN
#if HAS_CONSOLE
                              | RCC_APB2ENR_USART1EN
#endif
                              );
#else
  /* Enable GPIOA clock */
	rcc_periph_clock_enable(RCC_GPIOA);
  
  /* Enable GPIOB clock */
	rcc_periph_clock_enable(RCC_GPIOB);

  /* Enable AFIO clock */
	rcc_periph_clock_enable(RCC_AFIO);

  /* Enable DMA1 clock */
  rcc_periph_clock_enable(RCC_DMA1);

  /* Enable ADC1 clock */
  rcc_periph_clock_enable(RCC_ADC1);
#endif

  /* Disable JTAG only SWD, USE PD01 as GPIO */
  gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON, AFIO_MAPR_PD01_REMAP);
  
  sensor_init();
  driver_init();
  flowout_init();
  
#if HAS_CONSOLE
  console_init();
#endif

	systick_init();
  delay_init();
}

static void done(void) {
  systick_done();

#if HAS_CONSOLE
  console_done();
#endif

  flowout_done();
  driver_done();
  sensor_done();
  
  /* Deconfigure all GPIOs */
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);
  gpio_set_mode(GPIOB, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);

#if FAST_INIT
  rcc_peripheral_disable_clock(&RCC_AHBENR,
                               RCC_AHBENR_DMA1EN);
  rcc_peripheral_disable_clock(&RCC_APB2ENR,
                               RCC_APB2ENR_IOPAEN
                               | RCC_APB2ENR_IOPBEN
                               | RCC_APB2ENR_AFIOEN
                               | RCC_APB2ENR_ADC1EN
#if HAS_CONSOLE
                               | RCC_APB2ENR_USART1EN
#endif
                               );
#else
  /* Disable ADC1 clock */
  rcc_periph_clock_disable(RCC_ADC1);
  
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(RCC_DMA1);

  /* Disable AFIO clock */
	rcc_periph_clock_disable(RCC_AFIO);

  /* Disable GPIOB clock */
	rcc_periph_clock_disable(RCC_GPIOB);

  /* Disable GPIOA clock */
	rcc_periph_clock_disable(RCC_GPIOA);
#endif
}

/* main control */

typedef enum {
  control_off,
  control_manual,
  control_auto_dir,
  control_auto_pid,
} control_mode_t;

static ewma_param_t Swater_ewma_param;
static ewma_state_t Swater_ewma_state;

static pid_param_t pump_pid_param;
static pid_terms_t pump_pid_terms;

typedef enum {
  filter_none,
  filter_ewma,
  filter_kalman,
  filter_fir,
} filter_type_t;

static kalman_param_t Pwater_kalman_param = {
  1.0,
  1.0,
  0.1, /* measurement noise */
  10.0, /* environment noise */
};

static kalman_state_t Pwater_kalman_state = {
  0.0,
  0.1,
};

static ewma_param_t Pwater_ewma_param;
static ewma_state_t Pwater_ewma_state;

static const filter_value_t Pwater_filter_weight[] = {
  /*
  0.02481695761663131,
  0.12194351358652358,
  0.25892856788174673,
  0.3028244525145184,
  0.20456228782475214,
  0.07526185307939176,
  0.011662367496436135,
  */
  /*
  -0.03199845144919606,
  -0.12120903356463174,
  -0.24166776181170366,
  -0.3402537650952252,
  -0.34427021359049875,
  -0.19311049221441373,
  0.11903902687235637,
  0.5183930714232616,
  0.8687506956544258,
  1.0296996774479266,
  0.9263603391584642,
  0.5900786046327476,
  0.1449644519044048,
  -0.2524329732227202,
  -0.4854050830060199,
  -0.5216066668266296,
  -0.40992225366271645,
  -0.23940879792760997,
  -0.08959172962676977,
  -4.471843146602689e-17,
  0.03093741873623315,
  0.026993239705058663,
  0.012867655659944612,
  0.002793040803310775,
  */
  -4.692859668740605e-15,
  0.05967831961943446,
  0.25622890187304204,
  0.4250811195492479,
  1.4588653481104972e-16,
  -1.4573025356227327,
  -3.17923735019961,
  -3.1353746868789556,
  -1.3820265980918712e-15,
  4.660185242998563,
  7.091177540683413,
  4.979183812863739,
  2.3714649078224958e-15,
  -3.8570920966269147,
  -4.236748183169696,
  -2.1279395386410336,
  -9.50283844534603e-16,
  0.793433043292127,
  0.5724825028773255,
  0.1773355268775379,
  5.521523547975518e-17,
  -0.017255351814324756,
  -0.003836267681156421,
  5.847294661854109e-17,
};

#define Pwater_filter_length (sizeof(Pwater_filter_weight) / sizeof(filter_value_t))

static filter_value_t Pwater_filter_buffer[Pwater_filter_length];

static const filter_param_t Pwater_filter_param = {
  Pwater_filter_weight,
  Pwater_filter_buffer,
  Pwater_filter_length
};

static filter_state_t Pwater_filter_state = {
  0,
  0,
};

typedef enum {
  bench_none,
  bench_Twater_convert,
  bench_Pwater_convert,
  bench_Pwater_filter,
  bench_control_step,
} bench_target_t;

#define __params_c__
#include "params.h"

#define bench_begin(target)               \
  if (bench_target == bench_##target)     \
    bench_result = delay_snap()
#define bench_end(target)                      \
  if (bench_target == bench_##target)          \
    bench_result = delay_snap() - bench_result

monitor_def();

static bool throttle(int *cntp, int each){
  if(*cntp == 0){
    *cntp = each;
    return true;
  }
  (*cntp)--;
  return false;
}

#define each(n)                 \
  static int _each_##n##_ = 0;  \
  if(throttle(&_each_##n##_, n))

static pid_state_t pump_pid_state;

static inline void Pwater_filter_apply(void) {
  bench_begin(Pwater_convert);
  Pwater_raw = sensor_Pwater();
  bench_end(Pwater_convert);
  
  bench_begin(Pwater_filter);
  switch (Pwater_filter) {
  case filter_none:
    Pwater = Pwater_raw;
    break;
  case filter_kalman:
    Pwater = kalman_apply(&Pwater_kalman_state,
                          &Pwater_kalman_param,
                          Pwater_raw);
    break;
  case filter_ewma:
    Pwater = ewma_apply(&Pwater_ewma_state,
                        &Pwater_ewma_param,
                        Pwater_raw);
    break;
  case filter_fir:
    Pwater = filter_apply(&Pwater_filter_state,
                          &Pwater_filter_param,
                          Pwater_raw);
  }
  bench_end(Pwater_filter);
}

int main(void) {
  init();
  
  param_init(&params);
  monitor_init();
  pid_init(&pump_pid_state);

  uint32_t time;
  
  /* The monitor loop (Main thread) */
  for (time = 0; ; time ++) {
    monitor_wait();

    {
      float volume = flowout_stat();
      
      Vwater += volume;
      Swater = ewma_apply(&Swater_ewma_state,
                          &Swater_ewma_param,
                          volume * 6e3);
    }

#if 1
    Pwater_filter_apply();
#endif
    
    each(10) {
      /* Get new values from sensors */
      Vref = sensor_Vref();
      Tmcu = sensor_Tmcu();

      Tbox = sensor_Tbox();

      bench_begin(Twater_convert);
      Twater = sensor_Twater();
      bench_end(Twater_convert);

#if 0
      Pwater_filter_apply();
#endif

      bench_begin(control_step);
      switch (control_mode) {
      case control_off:
        pump_power = 0.0;
        break;
      case control_auto_dir:
        pump_power = (Pwater_target - Pwater) / Pwater_target;
        break;
      case control_auto_pid:
        pump_power = pid_step(&pump_pid_state,
                              &pump_pid_param,
                              &pump_pid_terms,
                              0.1,
                              Pwater_target,
                              Pwater);
        break;
      case control_manual:
        pump_power = manual_pump_power;
        break;
      }
      bench_end(control_step);

      if (pump_power < 0.0) {
        pump_power = 0.0;
      }

      if (pump_power > 1.0) {
        pump_power = 1.0;
      }
      
      driver_pump_power(pump_power);
    }
    
#if HAS_CONSOLE
    /* Handle UART communication */
    console_step(&params);
#endif
  }
  
  done();
  
  return 0;
}
