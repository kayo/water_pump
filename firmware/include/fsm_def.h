#ifndef _FSM_DEF_H
#define _FSM_DEF_H "fsm_def.h"

#define FSM

#ifndef FSM_EVENT_TYPE
#define FSM_EVENT_TYPE uint8_t
#endif

#ifndef FSM_EVENT_MATCH
#define FSM_EVENT_MATCH(_fsm_event_, _fsm_target_event_) ((_fsm_event_)==(_fsm_target_event_))
#endif

typedef void (*FSM_STATE_TYPE)(/*FSM_STATE_TYPE*/void **_fsm_state_, FSM_EVENT_TYPE _fsm_event_);

#define FSM_ENTER 0
#define FSM_LEAVE 1
#define FSM_EVENT(n) ((n) + 2)

#define FSM_CAT3_(a, b, c) a##b##c
#define FSM_CAT3(a, b, c) FSM_CAT3_(a, b, c)
#define FSM_NAME(_fsm_name_) FSM_CAT3(_fsm_, _fsm_name_, _)

#define FSM_STATE(_fsm_state_name_)                       \
  static void FSM_NAME(_fsm_state_name_)                  \
       (void **_fsm_state_p_, FSM_EVENT_TYPE _fsm_event_)

#define FSM_ACTION(_fsm_target_event_)                  \
  (void)_fsm_state_p_;                                  \
  if(FSM_EVENT_MATCH(_fsm_event_, _fsm_target_event_))

#define _fsm_none_ NULL

#define FSM_DEFINE(_fsm_machine_name_, _fsm_init_state_name_)   \
  static const FSM_STATE_TYPE FSM_NAME(_fsm_machine_name_##_) = \
    FSM_NAME(_fsm_init_state_name_);                            \
  FSM_STATE_TYPE FSM_NAME(_fsm_machine_name_) =                 \
    FSM_NAME(none);

#define FSM_SET(_fsm_machine_name_, _fsm_state_name_)       \
  FSM_NAME(_fsm_machine_name_) = FSM_NAME(_fsm_state_name_)

#define FSM_HANDLE(_fsm_machine_name_, _fsm_event_) \
  FSM_NAME(_fsm_machine_name_)((void**)&FSM_NAME(_fsm_machine_name_), _fsm_event_);

#define FSM_CHANGE(_fsm_machine_name_, _fsm_state_name_)  \
  FSM_HANDLE(_fsm_machine_name_, FSM_LEAVE);              \
  FSM_SET(_fsm_machine_name_, _fsm_state_name_);          \
  FSM_HANDLE(_fsm_machine_name_, FSM_ENTER);

#define FSM_SWITCH(_fsm_state_name_)                               \
  (*(FSM_STATE_TYPE*)_fsm_state_p_)(_fsm_state_p_, FSM_LEAVE);     \
  (*(FSM_STATE_TYPE*)_fsm_state_p_) = FSM_NAME(_fsm_state_name_);  \
  (*(FSM_STATE_TYPE*)_fsm_state_p_)(_fsm_state_p_, FSM_ENTER);

#define FSM_START(_fsm_machine_name_)                 \
  FSM_SET(_fsm_machine_name_, _fsm_machine_name_##_); \
  FSM_HANDLE(_fsm_machine_name_, FSM_ENTER);

#define FSM_STOP(_fsm_machine_name_)                  \
  FSM_HANDLE(_fsm_machine_name_, FSM_LEAVE);          \
  FSM_SET(_fsm_machine_name_, none);

#endif
