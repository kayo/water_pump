typedef float pid_value_t;

typedef struct {
  pid_value_t prev_err;
  pid_value_t int_acc;
} pid_state_t;

typedef struct {
  pid_value_t Kp;
  pid_value_t Ki;
  pid_value_t Kd;
  pid_value_t int_lim;
} pid_param_t;

typedef struct {
  pid_value_t prop;
  pid_value_t integ;
  pid_value_t deriv;
} pid_terms_t;

static inline void
pid_init(pid_state_t *state) {
  state->prev_err = 0;
  state->int_acc = 0;
}

static inline pid_value_t
pid_step(pid_state_t *state,
         const pid_param_t *param,
         pid_terms_t *terms,
         pid_value_t dt,
         pid_value_t setpoint,
         pid_value_t measured_value) {
  pid_value_t error = setpoint - measured_value;
  
  state->int_acc += error * dt;
  
  for (; state->int_acc > param->int_lim; state->int_acc -= param->int_lim);
  for (; state->int_acc < -param->int_lim; state->int_acc += param->int_lim);
  
  pid_value_t derivative = (error - state->prev_err) / dt;
  
  state->prev_err = error;

  terms->prop = param->Kp * error;
  terms->integ = param->Ki * state->int_acc;
  terms->deriv = param->Kd * derivative;
  
  return terms->prop + terms->integ + terms->deriv;
}
