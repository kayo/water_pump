typedef float filter_value_t;
typedef uint8_t filter_index_t;

typedef struct {
  const filter_value_t *weight;
  filter_value_t *buffer;
  filter_index_t length;
} filter_param_t;

typedef struct {
  filter_index_t index;
  filter_index_t count;
} filter_state_t;

static inline void
filter_init(filter_state_t *state) {
  state->count = state->index = 0;
}

static inline filter_value_t
filter_apply(filter_state_t *state,
             const filter_param_t *param,
             filter_value_t value) {
  param->buffer[state->index] = value;
  
  if (state->count < param->length) {
    state->count ++;
  }

  {
    filter_index_t count = 0;
    filter_index_t index = state->index;
    
    value = 0.0;

    for (; ; index --, count ++) {
      value += param->weight[count] * param->buffer[index];
      if (index == 0) {
        count ++;
        break;
      }
    }

    index = param->length - 1;
    
    for (; count < state->count; index --, count ++) {
      value += param->weight[count] * param->buffer[index];
    }
  }
  
  state->index ++;
  
  if (state->index == param->length) {
    state->index = 0;
  }

  return value;
}
