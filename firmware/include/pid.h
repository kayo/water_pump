/*
 * The set of template PID controlles implementations
 */

#define PID_CLASSIC_DEF(_impl_name_, _value_type_)                      \
  typedef struct {                                                      \
    union {                                                             \
      struct {                                                          \
        _value_type_ p_gain;  /* proportional gain */                   \
        _value_type_ d_gain;  /* derivative gain */                     \
        _value_type_ i_gain;  /* integral gain */                       \
        _value_type_ i_min;   /* integrator state minimum */            \
        _value_type_ i_max;   /* integrator state maximum */            \
        _value_type_ d_state; /* previous input value */                \
        _value_type_ i_state; /* integrator state */                    \
      };                                                                \
      _value_type_ values[7]; /* accessing to values using index */     \
    };                                                                  \
    unsigned step;                                                      \
  } pid_##_impl_name_##_t;                                              \
                                                                        \
  void pid_##_impl_name_##_init(pid_##_impl_name_##_t *pid);            \
                                                                        \
  void pid_##_impl_name_##_reset(pid_##_impl_name_##_t *pid);           \
                                                                        \
  _value_type_ pid_##_impl_name_##_step(pid_##_impl_name_##_t *pid,     \
                                        _value_type_ error,             \
                                        _value_type_ value)

#define PID_CLASSIC_IMPL(_impl_name_, _value_type_)                     \
  void pid_##_impl_name_##_init(pid_##_impl_name_##_t *pid) {           \
    unsigned char i;                                                    \
    for (i = 0; i < sizeof(pid->values)/sizeof(pid->values[0]); i++)    \
      pid->values[i] = 0;                                               \
    pid->step = 0;                                                      \
  }                                                                     \
                                                                        \
  void pid_##_impl_name_##_reset(pid_##_impl_name_##_t *pid) {          \
    pid->i_state = 0;                                                   \
    pid->step = 0;                                                      \
  }                                                                     \
                                                                        \
  _value_type_ pid_##_impl_name_##_step(pid_##_impl_name_##_t *pid,     \
                                        _value_type_ error,             \
                                        _value_type_ value) {           \
    _value_type_ output = value;                                        \
                                                                        \
    output += pid->p_gain * error; /* add the proportional term */      \
                                                                        \
    if (pid->step > 0) out += pid->d_gain * (value - pid->d_state);     \
    pid->d_state = value;                                               \
                                                                        \
    pid->i_state += error;        /* calc the integral state */         \
    /* limiting integral state */                                       \
    if (pid->i_state > pid->i_max) pid->i_state = pid->i_max;           \
    else if (pid->i_state < pid->i_min) pid->i_state = pid->i_min;      \
    output += pid->i_gain * i_state; /* add the integral term */        \
                                                                        \
    if (pid->step < 1) pid->step++;                                     \
    return out;                                                         \
  }

#define PID_OPTIMIZED_DEF(_impl_name_, _value_type_)                    \
  typedef struct {                                                      \
    union {                                                             \
      struct {                                                          \
        _value_type_ p_gain;   /* proportional gain */                  \
        _value_type_ d_gain;   /* discrete derivative gain */           \
        _value_type_ i_gain;   /* discrete integral gain */             \
        _value_type_ error[2]; /* two previous errors */                \
      };                                                                \
      _value_type_ values[6];                                           \
    };                                                                  \
    unsigned step;                                                      \
  } pid_##_impl_name_##_t;                                              \
                                                                        \
  void pid_##_impl_name_##_init(pid_##_impl_name_##_t *pid);            \
                                                                        \
  void pid_##_impl_name_##_reset(pid_##_impl_name_##_t *pid);           \
                                                                        \
  _value_type_ pid_##_impl_name_##_step(pid_##_impl_name_##_t *pid,     \
                                        _value_type_ error,             \
                                        _value_type_ value)

#define PID_OPTIMIZED_IMPL(_impl_name_, _value_type_)                   \
  void pid_##_impl_name_##_init(pid_##_impl_name_##_t *pid) {           \
    unsigned char i;                                                    \
    for (i = 0; i < sizeof(pid->values)/sizeof(pid->values[0]); i++)    \
      pid->values[i] = 0;                                               \
    pid->step = 0;                                                      \
  }                                                                     \
                                                                        \
  void pid_##_impl_name_##_reset(pid_##_impl_name_##_t *pid) {          \
    pid->step = 0;                                                      \
  }                                                                     \
                                                                        \
  _value_type_ pid_##_impl_name_##_step(pid_##_impl_name_##_t *pid,     \
                                        _value_type_ error,             \
                                        _value_type_ value) {           \
    value += pid->i_gain * error; /* add i_term */                      \
                                                                        \
    if (pid->step > 0) {                                                \
      value += pid->p_gain * (error - pid->error[1]); /* add p_term */  \
                                                                        \
      if (pid->step > 1) {                                              \
        value += pid->d_gain * (error + pid->error[2] /* add d_term */  \
                                - pid->error[1] - pid->error[1]);       \
      }                                                                 \
    }                                                                   \
                                                                        \
    pid->error[2] = pid->error[1];                                      \
    pid->error[1] = error;                                              \
                                                                        \
    if (pid->step < 2) pid->step ++;                                    \
    return value;                                                       \
  }
