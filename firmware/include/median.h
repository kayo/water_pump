typedef float median_value_t;
typedef uint8_t median_index_t;

typedef struct {
  median_value_t *buffer;
  median_index_t length;
} median_param_t;

typedef struct {
  median_index_t index;
  median_index_t count;
} median_state_t;

static inline void
median_init(median_state_t *state) {
  state->count = state->index = 0;
}

static inline median_value_t
median_apply(median_state_t *state,
             const median_param_t *param,
             median_value_t value) {
  param->buffer[state->index] = value;

  if (state->count < param->length) {
    state->count ++;
  }
  
  {
    median_index_t index = 0;
    value = 0;
    
    for (; index < state->count; index ++) {
      value += param->buffer[index];
    }
    
    value /= state->count;
  }

  state->index ++;
  
  if (state->index == param->length) {
    state->index = 0;
  }

  return value;
}
