void sensor_init(void);
void sensor_done(void);
void sensor_suspend(void);
void sensor_resume(void);

float sensor_Vref(void);
float sensor_Tmcu(void);
float sensor_Pwater(void);
float sensor_Twater(void);
float sensor_Tbox(void);
