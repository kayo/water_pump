/**
 * @brief Main Chip frequency
 *
 * The micro-controller unit main frequency in MHz.
 */
#define mcu_frequency 24

/**
 * @brief System tick divider and interval
 *
 * The divider for system tick.
 * Available values is 1 and 8.
 * Interval value in mS.
 */
#define systick_config 8,10

/**
 * @brief Water pressure sensor input
 */
#define water_press_input A,2

/**
 * @brief Water flow sensor input
 *
 * Timer and input channel
 */
#define water_flow_timer 2,1

/**
 * @brief Water pressure sensor adc
 */
#define water_press_adc 1,2

/**
 * @brief Water temperature sensor input
 */
#define water_therm_input A,1

/**
 * @brief Water temperature sensor adc
 */
#define water_therm_adc 1,1

/**
 * @brief Box temperature sensor input
 */
#define box_therm_input A,3

/**
 * @brief Box temperature sensor adc
 */
#define box_therm_adc 1,3

/**
 * @brief Zero-detector input
 */
#define zero_det_input B,0

/**
 * @brief Pump control output
 */
#define pump_ctrl_output B,1

/**
 * @brief Pump control timer
 */
#define pump_ctrl_timer 3,4

/**
 * @brief Serial console device number
 */
#define console_uart 1,38400,8,1

/**
 * @brief Serial console dma device and tx,rx channels
 */
#define console_dma 1,4,5

#define uart1_remap
