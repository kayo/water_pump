typedef float ewma_value_t;

typedef struct {
  ewma_value_t alpha;
} ewma_param_t;

typedef struct {
  ewma_value_t value;
} ewma_state_t;

#define ewma_alpha(N) ((ewma_value_t)2 / ((ewma_value_t)(N) + (ewma_value_t)1))

static inline ewma_value_t
ewma_apply(ewma_state_t *state,
           const ewma_param_t *param,
           ewma_value_t value) {
  return state->value = param->alpha * value + (1 - param->alpha) * state->value;
}
