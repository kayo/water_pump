typedef float kalman_value_t;

typedef struct {
  kalman_value_t f; /* factor of real value to previous real value */
  kalman_value_t h; /* factor of measured value to real value */
  kalman_value_t q; /* measurement noise */
  kalman_value_t r; /* environment noise */
} kalman_param_t;

typedef struct {
  kalman_value_t x; /* state */
  kalman_value_t p; /* covariance */
} kalman_state_t;

static inline kalman_value_t
kalman_apply(kalman_state_t *state,
             const kalman_param_t *param,
             kalman_value_t x) {
  kalman_value_t x0; /* predicted state */
  kalman_value_t p0; /* predicted covariance */
  kalman_value_t k;

  /* prediction */
  x0 = param->f * state->x;
  p0 = param->f * state->p * param->f + param->q;

  /* correction */
  k = param->h * p0 / (param->h * p0 * param->h + param->r);
  state->x = x0 + k * (x - param->h * x0);
  state->p = (1 - k * param->h) * p0;

  return state->x;
}
